package data.ex.demo;

public class VariableMethods {

	public int[] sortedArray(int[] arr) {

		int temparr = 0;

		for (int j = 0; j < arr.length - 1; j++) {

			for (int i = 0; i < arr.length - 1; i++) {

				if (arr[i] <= arr[i + 1]) {

				} else {
					temparr = arr[i];
					arr[i] = arr[i + 1];
					arr[i + 1] = temparr;
				}

			}
		}
		return arr;
	}

	public void returnArr(int[] arr) {
		
		System.out.print(" after sorting int numbers : [");
		for (int i = 0; i < arr.length; i++) {

			System.out.print(arr[i] + ",");

		}
		System.out.println( "]");
	}

	public double[] sortedArray(double[] arr) {

		double temparr = 0;

		for (int j = 0; j < arr.length - 1; j++) {

			for (int i = 0; i < arr.length - 1; i++) {

				if (arr[i] <= arr[i + 1]) {

				} else {
					temparr = arr[i];
					arr[i] = arr[i + 1];
					arr[i + 1] = temparr;
				}

			}
		}
		return arr;
	}
	public void returnArr(double[] arr) {
	
		System.out.print(" after sorting double numbers : [");
		for (int i = 0; i < arr.length; i++) {

			System.out.print(arr[i] + ",");

		}
		System.out.println( "]");
	}
	public long[] sortedArray(long[] arr) {

		long temparr = 0;

		for (int j = 0; j < arr.length - 1; j++) {

			for (int i = 0; i < arr.length - 1; i++) {

				if (arr[i] <= arr[i + 1]) {

				} else {
					temparr = arr[i];
					arr[i] = arr[i + 1];
					arr[i + 1] = temparr;
				}

			}
		}
		return arr;
	}
	public void returnArr(long[] arr) {
		
		System.out.print(" after sorting random numbers : [");
		for (int i = 0; i < arr.length; i++) {

			System.out.print(arr[i] + ",");

		}
		System.out.println( "]");

	}
}

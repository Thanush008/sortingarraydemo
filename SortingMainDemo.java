package data.ex.demo;

public class SortingMainDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		RandomNumGeneratorDemo rnd = new RandomNumGeneratorDemo();
		ArrayDecreasingOrder ado = new ArrayDecreasingOrder();
		VariableMethods vm = new VariableMethods();
		
		
		int arr[] = {24,23, 45, 12, 3, 21 };
		System.out.print("before sorting user input int numbers : [" );
		for (int i = 0; i < arr.length -1; i++) System.out.print(arr[i] + ",");
			System.out.println("]");
			vm.returnArr(vm.sortedArray(arr));
			ado.returnArr(ado.sortedArray(arr));
		
		
		double array[] = {2.3,4.0,9,738.908};
		System.out.print("before sorting user input double numbers : [" );
		for (int i = 0; i < array.length -1; i++) System.out.print(array[i] + ",");
			System.out.println("]");
		vm.returnArr(vm.sortedArray(array));
		
		vm.returnArr(vm.sortedArray(rnd.randomNumGenerater(5)));
		vm.returnArr(vm.sortedArray(rnd.randomNumGenerater(2.5)));
		
		
		
		
	}

}
